import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);
		int turn = 1;
		int counter = 0;
		while (counter < 9) {
			System.out.print("Player " + turn + " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player " + turn + " enter column number:");
			int col = reader.nextInt();
			if (checkOutOfRange(row, col)) {
				if (board[row - 1][col - 1] == ' ') {
					board[row - 1][col - 1] = turn == 1 ? 'X' : 'O';
					if (checkboard(board)) {
						printBoard(board);
						System.out.println("Player " + turn + " won!");
						break;
					}
				} else {
					System.out.println("Please try another coordinate");
					printBoard(board);
					continue;
				}

			} else {
				System.out.println("Please try another coordinate");
				printBoard(board);
				continue;
			}

			printBoard(board);
			counter = counter + 1;
			turn = turn == 1 ? 2 : 1;

		}
		if (!(checkboard(board))) {
			System.out.println("Games ended with a draw!");

		}
		reader.close();

	}

	public static boolean checkOutOfRange(int row, int col) {
		if (row <= 3 && row > 0 && col > 0 && col <= 3) {
			return true;
		}
		return false;

	}

	public static boolean checkboard(char[][] board) {
		for (int i = 0; i < 3; i++) {
			if ((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != ' ')) {
				return true;
			} else if ((board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != ' ')) {
				return true;
			}
		}
		if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] != ' ') {
			return true;
		} else if (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] != ' ') {
			return true;
		}
		return false;

	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}